import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { RacesComponent } from './races/races.component';
import { RaceService } from './race.service';
import { ApiService } from './api.service';
import { FakeRaceService } from './fake-race.service';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { PoniesComponent } from './ponies/ponies.component';
import { PonyComponent } from './pony/pony.component';
import { RaceComponent } from './race/race.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    RacesComponent,
    PoniesComponent,
    PonyComponent,
    RaceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ApiService,
    { provide: 'IS_PROD', useValue: false },
    {
      provide: RaceService,
      useFactory: (IS_PROD: boolean, apiService: ApiService) => IS_PROD ? new RaceService(apiService) : new FakeRaceService(),
      deps: ['IS_PROD', ApiService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

