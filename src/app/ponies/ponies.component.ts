import { Component, OnInit } from '@angular/core';
import { Pony } from 'app/models/pony.model';

@Component({
  selector: 'pr-ponies',
  templateUrl: './ponies.component.html',
  styleUrls: ['./ponies.component.css']
})
export class PoniesComponent {

  ponies: Array<Pony> = [
    { id: 1, name: 'Rainbow Dash', color: "blue" },
    { id: 2, name: 'Pinkie Pie' , color: "purple" }
    ];

}
