import { Component, OnInit } from '@angular/core';
import { RaceService } from './race.service';
import { Title } from '@angular/platform-browser';
import { RaceModel } from './models/race.model';

@Component({
  selector: 'pr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor(title: Title){
    title.setTitle('Super Ponyracer');
  }

  title = 'pr works!';
}
