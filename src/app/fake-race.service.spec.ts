import { TestBed, inject } from '@angular/core/testing';

import { FakeRaceService } from './fake-race.service';

describe('FakeRaceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FakeRaceService]
    });
  });

  it('should ...', inject([FakeRaceService], (service: FakeRaceService) => {
    expect(service).toBeTruthy();
  }));
});
