import { Pony } from "./pony.model";


export class RaceModel{
 id: number;
 name : string;
 ponies: Pony[];
 startInstant: string;
}
