import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import { Observable } from 'rxjs/Observable';
import { RaceModel } from './models/race.model';

@Injectable()
export class RaceService {

  constructor(private apiService : ApiService) { }

  list() : Observable<Array<RaceModel>> {
    return this.apiService.get('/races');
  }

}
