import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pony } from 'app/models/pony.model';

@Component({
  selector: 'pr-pony',
  templateUrl: './pony.component.html',
  styleUrls: ['./pony.component.css']
})
export class PonyComponent {

  @Input() pony: Pony;

  @Output() ponySelected = new EventEmitter<Pony>();

  selectPony() {
    this.ponySelected.emit(this.pony);
  }

}
